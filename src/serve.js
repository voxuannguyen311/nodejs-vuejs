const path = require('path');
const express = require('express');
// morgan giúp chúng ta không cần phải reset lại trang để code chạy
const morgan = require('morgan');
const { engine } = require('express-handlebars');
const app = express();
const port = 3000;
// hổ trợ dùng meethod put path
const methodOverride = require('method-override')
app.use(methodOverride('_method'))

const mime = require('mime');

// connect to DB
const db = require('./config/db');
db.connect();

 
// hquan sát request
app.use(morgan('combined'));
// template engine
app.engine(
  'hbs',
  engine({
    extname: '.hbs',
  })
);
app.set('view engine', 'hbs');
// dùng thư viện path của nodejs nó có thẻ trả về những việc liên quan đến đường dẫn của chúng ta  path.join(__dirname, 'resources/views'))
const viewsPaths = [
  path.join(__dirname, 'resources/views/admin'),
  path.join(__dirname, 'resources/views/user')
];

app.set('views', viewsPaths);



// Middleware 
// ham số extended: true được sử dụng để bật tính năng parsing của JSON body với độ sâu lớn hơn 1 level
app.use(express.urlencoded({
  extended: true
}));
//  Middleware này được sử dụng để phân tích các thông tin đến từ yêu cầu HTTP được gửi từ trình duyệt, với kiểu dữ liệu application/x-www-form-urlencoded. Các thông tin này sẽ được trích xuất và đưa vào đối tượng req.body trong ExpressJS.
app.use(express.json());
// Middleware này được sử dụng để phân tích các thông tin đến từ yêu cầu HTTP được gửi từ trình duyệt, với kiểu dữ liệu application/json. Các thông tin này sẽ được trích xuất và đưa vào đối tượng req.body trong ExpressJS.

const route = require('./routes');
// Routes init
route(app);

// 


// Cấu hình đường dẫn tĩnh để cung cấp các tệp CSS
app.use('/public', express.static(path.join(__dirname, 'public'), {
  setHeaders: (res, filePath) => {
    res.setHeader('Content-Type', mime.getType(filePath));
  },
}));


app.use(express.static(__dirname + '/public'));
// 
app.listen(port, () => {
  console.log(`http://localhost:${port}`);
});
