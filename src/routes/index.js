const adminsRoute = require('./admin');



function route(app){
    // admin
    app.use('/', adminsRoute);
}


module.exports = route;